import numpy as np
import xarray as xr
import regionmask
from scipy import signal
from typing import Union, Tuple
import matplotlib.pyplot as plt


xtype = Union[xr.Dataset, xr.DataArray]


class Operators:

    def __init__(self, data: xtype):
        """
        Operators that can be applied to xarray datasets

        Parameters
        ----------
        data :
            xarray of data to be analyzed
        """

        self._data = data

        # convert common dimension names to cf-1.7 conventions
        self.coord_map = {}
        for coord in data.coords.keys():
            if 'longitude' == coord.lower():
                self._data = self._data.rename({coord: 'lon'})
                self.coord_map['lon'] = coord
            if 'latitude' == coord.lower():
                self._data = self._data.rename({coord: 'lat'})
                self.coord_map['lat'] = coord
            if 'time' == coord.lower():
                self._data = self._data.rename({coord: 'time'})
                self.coord_map['time'] = coord
            if 'pressure' == coord.lower():
                self._data = self._data.rename({coord: 'plev'})
                self.coord_map['plev'] = coord

    @property
    def data(self):

        data = self._data.copy()
        # some keys may no longer be present due to averaging
        for key in self.coord_map:
            try:
                data = data.rename({key: self.coord_map[key]})
            except ValueError:
                pass

        return data

    def area_average(self, lat_bin: Tuple[float, float] = (-90, 90), lon_bin: Tuple[float, float] = (0, 360)):
        """
        Compute the area weighted average. Note that this code assumes a rectilinear grid and points are approximated
        as squares rather than the true quadrilaterals

        TODO: Impliment other grids and more accurate computation using SphericalVoronoi, xgcm, and/or xesmf

        Parameters
        ----------
        lat_bin:
            latitudinal region of interest
        lon_bin:
            longitudinal region of interest
        """

        min_lat, max_lat = lat_bin
        min_lon, max_lon = lon_bin

        if (min_lon > 0) & (max_lon < 0):
            max_lon = 360 + max_lon

        wrap_lons = False
        if min_lon < 0:  # shift longitudes to -180 to 180
            wrap_lons = True
            self._data = self._data.assign_coords(lon=(((self._data.lon + 180) % 360) - 180))

        # select the bin we are interested in
        self._data = self._data.sortby('lat')
        self._data = self._data.sel(lat=slice(min_lat, max_lat))

        if 'lon' in self._data.coords.keys():
            self._data = self._data.sortby('lon')
            self._data = self._data.sel(lon=slice(min_lon, max_lon))

        weights = np.cos(self._data.lat * np.pi / 180)
        weights = weights.where(np.isfinite(self._data))
        weights = weights.fillna(0.0)
        self._data = self._data.fillna(0.0)

        if 'lon' in self._data.coords.keys():
            self._data = (self._data * weights).sum(dim=['lat', 'lon']) / weights.sum(dim=['lat', 'lon'])
        else:
            self._data = (self._data * weights).sum(dim='lat') / weights.sum(dim='lat')

        return Operators(self.data)

    def deseasonalize(self):
        """
        Deseasonalize the data, subtracting the climatological monthly data
        """
        self._data = self._data.groupby('time.month') - self._data.groupby('time.month').mean(dim='time')
        return Operators(self.data)

    def sample_season(self, season: str = 'all'):
        """
        Bin the data into seasons and return the season of interest

        Parameters
        ----------
        season :
            Name of the season to be returned. Defaults to all
        """

        self._data = self._data.resample(time='QS-DEC').mean(dim='time')
        season = season.lower()

        if season == 'djf':
            month = 12
        elif season == 'mam':
            month = 3
        elif season == 'jja':
            month = 6
        elif season == 'son':
            month = 9
        elif season == 'all':
            pass
        else:
            raise ValueError('unknown season')

        if season != 'all':
            self._data = self._data.where(self._data['time.month'] == month, drop=True)

        return Operators(self.data)

    def remove_base(self, base_period: int = 30, start_year: int = None, min_period: int = 0):
        """
        Remove the mean of the data in every `base_period`. This is typically done for indices like ENSO.

        Parameters
        ----------
        base_period :
            length of the base periods
        start_year :
            Beginning of a base period. Should be before the first time.
        min_period :
            If the number of years in the last base period is less than this it will be combined with the previous
        """

        if start_year is None:
            start_year = int(str(self._data.time.values[0]).split('-')[0])

        if int(str(self._data.time.values[0]).split('-')[0]) < start_year:
            raise ValueError('start_year should be before than the first time of the dataset')

        end_year = int(str(self._data.time.values[-1]).split('-')[0])
        years = [y for y in range(start_year, end_year+1, base_period)]
        if years[-1] <= end_year:
            years.append(end_year+1)

        if years[-1] - years[-2] < min_period:
            years.pop(-2)

        norm_data = []
        for (start, end) in zip(years[0:-1], years[1:]):
            temp = self._data.sel(time=slice(f'{start}-01-01', f'{end - 1}-12-31'))
            norm_data.append(temp - temp.mean(dim='time'))

        self._data = xr.concat(norm_data, dim='time')
        return Operators(self.data)

    def detrend(self, dim: str = 'time', **kwargs):

        self._data.values = signal.detrend(self._data.values, axis=self._data.get_axis_num(dim=dim), **kwargs)
        return Operators(self.data)

    def region_mask(self, region: str):
        """
        Mask the data to only selected region

        Parameters
        ----------
        region :
            Name of the desired region
        """

        wrap_lons = False
        if self._data.lon.max() > 180:
            wrap_lons = True
            self._data = self._data.assign_coords(lon=(((self._data.lon + 180) % 360) - 180)).sortby('lon')

        if region == 'land':
            mask = regionmask.defined_regions.natural_earth.land_110.mask(self._data)
            self._data = self._data.where(np.isfinite(mask))

        elif region == 'ocean':
            mask = regionmask.defined_regions.natural_earth.land_110.mask(self._data)
            self._data = self._data.where(~np.isfinite(mask))

        elif region in regionmask.defined_regions.giorgi.names:
            mask = regionmask.defined_regions.giorgi.mask(self._data)
            mask_id = regionmask.defined_regions.giorgi.map_keys(region)
            self._data = self._data.where(mask == mask_id)

        elif region in regionmask.defined_regions.natural_earth.countries_110.names:
            mask = regionmask.defined_regions.natural_earth.countries_110.mask(self._data)
            mask_id = regionmask.defined_regions.natural_earth.countries_110.map_keys(region)
            self._data = self._data.where(mask == mask_id)

        else:
            raise ValueError('region is not recognized')

        if wrap_lons:
            self._data = self._data.assign_coords(lon=((self._data.lon + 360) % 360)).sortby('lon')

        return Operators(self.data)

    def land_mask(self):
        """
        Return data over land
        """
        return self.region_mask('land')

    def ocean_mask(self):
        """
        Return data over ocean
        """
        return self.region_mask('ocean')
