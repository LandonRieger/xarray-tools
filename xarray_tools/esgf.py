import xarray as xr
import os
from pyesgf.search import SearchConnection
from pyesgf.search.exceptions import EsgfSearchException
import requests
import appdirs
from urllib.request import urlretrieve
import urllib
from typing import List, Union
import time


class DirectorySturcture:

    def __init__(self,
                 mip_era: str = '*',
                 activity: str = '*',
                 institution: str = '*',
                 source: str = '*',
                 experiment: str = '*',
                 variant: str = '*',
                 table: str = '*',
                 variable: str = '*',
                 grid: str = '*',
                 version: str = '*'):

        """
        Helper class to get and set file directory structure according CMIP guidelines.

        Parameters
        ----------
        mip_era
        activity
        institution
        source
        experiment
        variant
        table
        variable
        grid
        version
        """
        self.mip_era = mip_era
        self.activity = activity
        self.institution = institution
        self.source = source
        self.experiment = experiment
        self.variant = variant
        self.table = table
        self.variable = variable
        self.grid = grid
        self.version = version

    @classmethod
    def from_path(cls, path: str):
        pathlist = os.path.normpath(path).split(os.sep)
        return cls(*pathlist)

    @classmethod
    def from_dataset_id(cls, id: str):
        id = id.split('|')[0]
        return cls(*id.split('.'))

    @property
    def directory(self):
        return os.path.join(self.mip_era, self.activity, self.institution, self.source, self.experiment,
                            self.variant, self.table, self.variable, self.grid, self.version)

    def as_dict(self):
        return {'mip_era': self.mip_era, 'activity': self.activity, 'institution': self.institution,
                'source': self.source, 'experiment': self.experiment, 'variant': self.variant,
                'table': self.table, 'variable': self.variable, 'grid': self.grid, 'version': self.version}


class File:

    def __init__(self, filename: str, directory: str = None, cmip_folder: str = None):
        self.filename = filename
        self.download_urls = []
        self.opendap_urls = []
        self.directory = directory
        if cmip_folder is None:
            cmip_folder = get_cmip_folder()
        self.cmip_folder = cmip_folder

    def __repr__(self):
        return self.filename

    @property
    def exists(self):
        return os.path.isfile(self.path)

    @property
    def path(self):
        return os.path.normpath(os.path.join(self.cmip_folder, self.directory, self.filename))

    def add_download_url(self, url):
        if url not in self.download_urls:
            self.download_urls.append(url)

    def add_opendap_url(self, url):
        if url not in self.opendap_urls:
            self.opendap_urls.append(url)

    def download(self):
        if len(self.download_urls) == 0:
            raise ValueError('download_url must have at least one member')

        for url in self.download_urls:
            try:
                r = urlretrieve(url, self.path)
            except urllib.error.HTTPError as err:
                pass
            else:
                break
        else:
            print(f'error downloading {self.filename}')


class ESGFDataset:

    def __init__(self, **kwargs):

        self.search_node = 'https://esgf-node.llnl.gov/esg-search'
        self.skip_nodes = ()
        self.cmip_folder = get_cmip_folder()
        self.max_attempt = 3
        self.opendap = False
        self.first_node_only = False
        self.distrib = True

        self._conn = None
        self._files = {}
        self._results = None
        self._kwargs = kwargs
        self._data = None

    @property
    def search_params(self):
        return self._kwargs

    @search_params.setter
    def search_params(self, value):
        self._kwargs = value

    @property
    def conn(self):
        if self._conn is None:
            self._conn = SearchConnection(self.search_node, distrib=self.distrib)
        return self._conn

    @property
    def files(self):
        if self._files:
            return self._files

        # TODO: some results are repeated so could skip if all nodes have the same files and can guarantee download
        ids = []
        for result in self.results:

            if self.first_node_only:
                if result.dataset_id.split('|')[0] in ids:
                    continue

            ids.append(result.dataset_id.split('|')[0])
            if result.dataset_id.split('|')[1] in self.skip_nodes:
                continue
            folder_struct = DirectorySturcture.from_dataset_id(result.dataset_id)
            for i in range(self.max_attempt):
                try:
                    print(result.file_context().facet_constraints)
                except EsgfSearchException:
                    time.sleep(0.5)
                else:
                    break

            for f in result.file_context().search():
                if f.filename not in self._files.keys():
                    self._files[f.filename] = File(f.filename,
                                                   directory=folder_struct.directory,
                                                   cmip_folder=self.cmip_folder)
                self._files[f.filename].add_download_url(f.download_url)
                self._files[f.filename].add_opendap_url(f.opendap_url)

        return self._files

    @property
    def file_list(self):
        return list(self.files.keys())

    @property
    def results(self):
        if self._results is None:
            for attemp in range(self.max_attempt):
                try:
                    ctx = self.conn.new_context(**self._kwargs)
                    self._results = ctx.search()
                except (requests.HTTPError, requests.ConnectionError) as err:
                    print('Retrying due to: ', err)
                    attemp += 1
                else:
                    break
        return self._results

    def download_data(self):
        for filename, file in self.files.items():
            if not file.exists:
                os.makedirs(os.path.dirname(file.path), exist_ok=True)
                file.download()

    @property
    def data(self) -> Union[xr.Dataset, List[xr.Dataset]]:

        if self._data is not None:
            return self._data

        self.download_data()

        datasets = {}
        for name, file in self.files.items():
            if file.directory not in datasets.keys():
                datasets[file.directory] = []
            datasets[file.directory].append(file.path)

        data = []
        keys = {}
        for name, dataset in datasets.items():
            ds = xr.open_mfdataset(list(set(dataset)), combine='by_coords')
            for key, val in DirectorySturcture.from_path(name).as_dict().items():
                ds[key] = val
                try:
                    keys[key].append(val)
                except KeyError:
                    keys[key] = [val]
            data.append(ds)

        if len(data) == 1:
            self._data = data[0]
        else:
            multi_vars = []
            for key in keys:
                if key == 'variable':  # variable concatenation works without issue
                    pass
                if len(set(keys[key])) > 1:
                    multi_vars.append(key)

            # concatenate along single dimension or return list of datasets
            if len(multi_vars) == 1:
                self._data = xr.concat(data, dim=multi_vars[0])
            else:
                self._data = data

        return self._data


def get_cmip_folder():

    try:
        cmip_folder = os.environ['CMIP_DATA_FOLDER']
    except KeyError:
        cmip_folder = appdirs.user_cache_dir('xarray_tools')

    return cmip_folder
