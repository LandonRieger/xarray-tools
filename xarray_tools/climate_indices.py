from xarray_tools.operators import Operators
import xarray as xr


def enso(ts: xr.DataArray, region: str = '3.4') -> xr.DataArray:
    """
    Compute the ENSO 3.4 index from a CMIP6 model dataset

    Parameters
    ----------
    ts :
        Surface temperature
    region :
        Nino region, one of '3.4, 3, 4, 1+2', default = 3.4
    """

    # definition of mid and north-atlantic regions
    nino_lat = (-5, 5)
    if region == '3.4':
        nino_lon = (190, 240)
    elif region == '3':
        nino_lon = (210, 270)
    elif region == '4':
        nino_lon = (160, 210)
    elif region == '1+2':
        nino_lat = (-10, 0)
        nino_lon = (270, 280)

    # subset and average the data
    print('computing enso index')
    ts = Operators(ts).area_average(nino_lat, nino_lon) \
                      .deseasonalize() \
                      .remove_base(30, min_period=15).data
    return ts.rename('nino34')


def nao(zg: xr.DataArray, plev: float = 50000) -> xr.DataArray:
    """
    Compute the North Atlantic Oscillation from a CMIP6 model dataset

    Parameters
    ----------
    zg :
        Geopotential height of pressure levels
    plev :
        pressure level used to compute nao. If psl is used then pressure level
        is not used.
    """

    # definition of mid and north-atlantic regions
    ma_lat = (20, 55)
    ma_lon = (-90, 60)
    na_lat = (55, 90)
    na_lon = (-90, 60)

    # subset and average the data
    print('computing nao index')
    if 'plev' in zg.coords.keys():
        zg = zg.sel(plev=plev, method='nearest')
    ma_t = Operators(zg).area_average(ma_lat, ma_lon).data
    na_t = Operators(zg).area_average(na_lat, na_lon).data

    nao = (ma_t - na_t).rename('nao')
    return nao - nao.mean(dim='time')


def amoc(msftmz: xr.DataArray) -> xr.DataArray:
    """
    Atlantic Meridional Overturning Circulation

    Parameters
    ----------
    msftmz :
        Ocean meridional overturning mass streamfunction
    """

    if 'basin' in msftmz.coords.keys():
        msftmz = msftmz.sel(basin=0)

    amoc = msftmz.sel(lat=26.5, method='nearest') \
                 .max(dim='lev').load().rename('amoc')

    return amoc - amoc.mean(dim='time')
