from xarray_tools.operators import Operators
from xarray_tools.esgf import ESGFDataset, get_cmip_folder

from ._version import get_versions
__version__ = get_versions()['version']
del get_versions
