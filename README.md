# Climate Analysis Tools (xarray_tools)

Some xarray tools to help with analyzing climate data. These are mostly some simple
wrappers around functions I commonly use for a consistent interface and to 
allow for operator chaining.


## Installation

```bash
pip install git+ssh://git@gitlab.com/LandonRieger/xarray-tools.git
```

## Basic Use
### Operators

```python
from xarray_tools import Operators
import xarray as xr
import matplotlib.pyplot as plt

# load some toy climate data
air_temp = xr.tutorial.load_dataset('air_temperature').air

# plot the deseasonalized and detrended 'djf' air temperature data over Greenland
ave_temp = Operators(air_temp).region_mask('Greenland') \
                              .land_mask() \
                              .deseasonalize() \
                              .area_average() \
                              .detrend() \
                              .sample_seaons('djf').data
ave_temp.plot()

# save the data
ave_temp.to_netdf('air_temp_processed.nc')
```

### Opening data from ESGF

Load CMIP data from the ESGF server into an xarray dataset. If the data is not available
it will be downloaded, if it is already present it will be loaded from disc. The location 
of the local CMIP archive is specified by the system environment variable `CMIP_DATA_FOLDER`.
If the `CMIP_DATA_FOLDER` is not set a cache directory is used. You can see the current
storage directory using:

```python
from xarray_tools import get_cmip_folder
print(get_cmip_folder())
```
Search parameters are passed to the 
[`pyesgf.new_context`](https://esgf-pyclient.readthedocs.io/en/latest/notebooks/examples/search.html) function so can
accept all the same variables. For exmaple, to download CanESM5 historical temperature data from a single variant:
```python
from xarray_tools import ESGFDataset

# download historical surface temperature data (if its not already) 
# and return in xarray
esgf = ESGFDataset(project='CMIP6',
                   source_id='CanESM5',
                   experiment_id='historical',
                   variable='ts',
                   table_id='Amon',
                   variant_label='r1i1p2f1')
ts = esgf.data
```
If more than one dataset meets the criteria an attempt is made at merging into a dataset, however
if this is not possible data will be returned as a list.

If you would like to see what files are available before downloading
```python
from xarray_tools import ESGFDataset
esgf = ESGFDataset(project='CMIP6',
                   source_id='CanESM5',
                   experiment_id='historical',
                   variable='ts',
                   table_id='Amon')
files = esgf.file_list
```

### Climate Indices

```python
from xarray_tools import climate_indices as ci
import xarray as xr

enso = ci.enso(ts.ts, '3.4')
```