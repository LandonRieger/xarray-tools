from xarray_tools import ESGFDataset, get_cmip_folder
import xarray as xr
import appdirs
import os
import shutil


def test_open_esgf_dataset():

    test_folder = os.path.join(os.path.dirname(__file__), 'test_cmip_data')
    os.environ['CMIP_DATA_FOLDER'] = test_folder
    psl = ESGFDataset(project='CMIP6',
                      source_id='CanESM5',
                      experiment_id='pdSST-pdSIC',
                      variable='psl',
                      table_id='Amon',
                      variant_label='r1i1p2f1')
    psl.search_node = 'https://esgf-index1.ceda.ac.uk/esg-search'
    psl.first_node_only = True

    assert type(psl.data) == xr.Dataset
    shutil.rmtree(test_folder, ignore_errors=True)
    del os.environ['CMIP_DATA_FOLDER']


def test_cmip_folder():

    assert get_cmip_folder() == appdirs.user_cache_dir('xarray_tools')
    os.environ['CMIP_DATA_FOLDER'] = os.path.dirname(__file__)
    assert get_cmip_folder() == os.path.dirname(__file__)


if __name__ == '__main__':
    test_open_esgf_dataset()
    test_cmip_folder()
