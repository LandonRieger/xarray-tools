import xarray_tools.climate_indices as ci
import pandas as pd
import xarray as xr
import numpy as np


def generate_data():

    lat = np.arange(-90, 91, 2.5)
    lon = np.linspace(0, 360, 10)
    time = pd.date_range('2000', '2100', freq='MS')
    data = xr.DataArray(np.ones((len(lat), len(lon), len(time))),
                        coords=[lat, lon, time],
                        dims=['lat', 'lon', 'time'])
    return data


def generate_amoc_data():

    lat = np.arange(-90, 91, 2.5)
    lon = np.linspace(0, 360, 10)
    time = pd.date_range('2000', '2100', freq='MS')
    plev = np.arange(0, 10, 1)
    data = xr.DataArray(np.ones((len(lat), len(lon), len(time), len(plev))),
                        coords=[lat, lon, time, plev],
                        dims=['lat', 'lon', 'time', 'lev'])
    return data


def test_enso():

    data = generate_data()
    enso = ci.enso(data, '3.4')
    assert enso.sum().values == 0.0
    enso = ci.enso(data, '3')
    assert enso.sum().values == 0.0
    enso = ci.enso(data, '4')
    assert enso.sum().values == 0.0
    enso = ci.enso(data, '1+2')
    assert enso.sum().values == 0.0


def test_nao():

    data = generate_data()
    nao = ci.nao(data)
    assert nao.sum().values == 0.0


def test_amoc():

    data = generate_amoc_data()
    amoc = ci.amoc(data)
    assert amoc.sum().values == 0.0


if __name__ == '__main__':
    test_enso()
    test_nao()
    test_amoc()
