from setuptools import setup, find_packages
import versioneer

setup(name='xarray_tools',
      version=versioneer.get_version(),
      cmdclass=versioneer.get_cmdclass(),
      description='Some tools for climate analysis using xarray.',
      author='Landon Rieger',
      author_email='landon.rieger@usask.ca',
      license='MIT',
      url='https://gitlab.com/LandonRieger/xarray_tools.git',
      packages=find_packages(),
      package_data={'data': ['config_example.yaml']},
      install_requires=['netCDF4', 'dask', 'xarray', 'cftime', 'matplotlib',
                        'nc-time-axis', 'numpy', 'pandas', 'regionmask', 'scipy',
                        'appdirs', 'esgf-pyclient'],
      )
